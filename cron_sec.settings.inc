<?php
/**
 * @file
 * Include file for cron_sec's system settings.
 */

/**
 * Systems settings form.
 *
 * @return array
 *   The system settings form array.
 */
function cron_sec_settings_form() {

  $form = array();

  $form['cron_sec_user'] = array(
    '#type' => 'textfield',
    '#title' => t('CRON user'),
    '#default_value' => variable_get('cron_sec_user', ''),
    '#autocomplete_path' => 'user/autocomplete',
    '#description' => t('Choose the user you wish to be the active user when CRON runs.  This
          should be a user specifrically configured for running only CRON, set up with propper
          permissions.  This will allow you to run CRON at an elevated level over the anonymous
          user, which may be userful for other modules that may not operate correctly with CRON
          running at a low permissions level.  For example, link checker with the revisions
          module will potentially result in unpublished nodes with corrections made by link
          checker.  Leave blank to run in Drupal\'s default mode.'),
  );

  $form['cron_sec_key'] = array(
    '#type' => 'textfield',
    '#title' => t('CRON Security Key'),
    '#default_value' => variable_get('cron_sec_key', ''),
    '#description' => t('Indicate the security key you would like to use for CRON runs.  If
          the key provided in the URL does not match this key, CRON will abort.  The key should
          be a sequence of characters and numbers only, and at least 15 characters long.'),
    '#element_validate' => array('cron_sec_key_validate'),
  );

  $form['cron_sec_key_regenerate'] = array(
    '#type' => 'checkbox',
    '#title' => (empty($form['cron_sec_key']['#default_value']) ? t('Generate a random key') : t('Regenerate a random key')),
    '#description' => (empty($form['cron_sec_key']['#default_value']) ? t('Check
          this box to generate a random security key.') : t('Check this box to
          regenerate a random security key.')) . ' ' . t('After you click the
          <b>Save configuration</b> button, you will see the key, which you will need
          to make sure matches any enternal (eg., crontab generated) call to cron.php .'),
  );

  $module = cron_sec_weight(FALSE, FALSE, 'module');
  if ($module != 'cron_sec') {
    $form['cron_sec_fix_weight'] = array(
      '#type' => 'checkbox',
      '#title' => t("Fix CRON Security's weight"),
      '#default_value' => TRUE,
      '#description' => t("Check this box to fix CRON Security's module
            weight.  !module currently has a weight lighter than CRON Security, which
            must be the 'lightest' module in order that is executes first to check security,
            and exit if cron is executed without the correct security settings, or otherwise
            ensure the correct user is used should cron be executed.  CRON Security should
            be lighter than all other modules, including other cron management modules.",
              array('!module' => $module)),
    );
  }

  $form['#submit'] = array('cron_sec_system_settings_form_submit');

  return system_settings_form($form);

}

/**
 * Key validation.
 *
 * Ensure user-supplied key contains appropriate characters and is at least 15
 * characters long.
 *
 * @param array $element
 *   The form-element to test.
 * @param array $form_state
 *   The state of the submitted form.
 */
function cron_sec_key_validate($element, &$form_state) {
  if (!empty($element['#value'])) {
    if (preg_match('/[^0-9a-z]/i', $element['#value'])) {
      form_error($element, t('Key values can only contain upper and lower case letters and digits.'));
    }
    if (drupal_strlen($element['#value']) < 15) {
      form_error($element, t('Keys need to be at least 15 alpha-numberic characaters long.'));
    }
  }
}

/**
 * Submit handler.
 *
 * Specifically deal with the 'generate key' and 'reset weight'
 * checkboxes on the system settings form.
 *
 * @param array $form
 *   The original form array.
 * @param array $form_state
 *   The state of the submitted form.
 */
function cron_sec_system_settings_form_submit($form, &$form_state) {

  if ($form_state['values']['cron_sec_key_regenerate']) {
    $form_state['values']['cron_sec_key'] = _cron_sec_random_key();
  }
  // If the security key changed, display a message about the new one.
  if (!empty($form_state['values']['cron_sec_key']) &&
            $form_state['values']['cron_sec_key'] != $form['cron_sec_key']['#default_value']) {
    drupal_set_message(t('New CRON Security key: !key .  Make sure you update your crontab or other mechanisms
        for externally initiating CRON.  For example: wget -O - -q -t 1 !site/cron.php?key=!key',
        array(
          '!key' => $form_state['values']['cron_sec_key'],
          '!site' => $GLOBALS['base_url'],
        )));

  }
  unset($form_state['values']['cron_sec_key_regenerate']);

  // Won't be set if the weight was already the lightest.
  if (isset($form_state['values']['cron_sec_fix_weight'])) {
    if ($form_state['values']['cron_sec_fix_weight']) {
      cron_sec_weight(TRUE);
    }
    unset($form_state['values']['cron_sec_fix_weight']);
  }

}

/**
 * Generate a random sequence of charcters to be used as a key.
 *
 * @param int $length
 *   Length of the string to generate.  Defaults to 20.
 *
 * @return string
 *   The randomly-generated key.
 */
function _cron_sec_random_key($length = 20) {

  $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
  $key = '';
  for ($i = 0; $i < $length; $i++) {
    $key .= $characters[mt_rand(0, 61)];
  }
  return $key;
}
