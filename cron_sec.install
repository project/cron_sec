<?php
/**
 * @file
 * Install file for cron_sec.
 */

/**
 * Implements hook_requirements().
 */
function cron_sec_requirements($phase) {
  $requirements = array();
  // Ensure translations don't break at install time.
  $t = get_t();

  // Report CRON Security's weight.
  if ($phase == 'runtime') {
    $module = db_result(db_query('SELECT name FROM {system} WHERE status = 1 ORDER BY weight LIMIT 1'));
    if ($module == 'cron_sec') {
      $requirements['cron_sec']['value'] = $t('CRON Security is the lightest module,
                which it needs to be inorder to function properly.');
    }
    else {
      $requirements['cron_sec'] = array(
        'description' => $t('You should <a href="@reset">reset CRON Security\'s weight</a> in
                order for it to function properly.  Otherwise, results may be unpredictable.',
                array('@reset' => url('admin/build/cron_sec/reset_weight'))),
        'severity' => REQUIREMENT_ERROR,
        'value' => $t('CRON Security is <b>NOT</b> the lightest module, which it needs
                to be inorder to function properly.'),
      );
    }

    $requirements['cron_sec']['title'] = $t('CRON Security Module Weight');
  }

  return $requirements;
}

/**
 * Implements hook_install().
 */
function cron_sec_install() {

  $t = get_t();
  module_load_include('module', 'cron_sec');
  cron_sec_weight(TRUE);
  drupal_set_message($t('CRON Security has been set to be the "lightest" module
        so that it will execute first when cron is run.  Don\'t forget to create
        a cron user and appropriate role for that user, and then set that user
        to run cron in <a href="@link">CRON Security\'s system settings</a>.',
        array('@link' => '/admin/settings/cron_sec')));

}

/**
 * Implements hook_uninstall().
 */
function cron_sec_uninstall() {

  variable_del('cron_sec_user');
  variable_del('cron_sec_key');

}
