CRON Security (cron_sec)
************************

cron_sec, when enabled as the lightest module, implements key security similar
to Drupal 7's enhanced key security, and optionally allows cron to run at an
elevated permission level thought the use of switching the active user for the
session to one specified in its configuration, and then switching the user back
as the cron session ends to the anonymous user.

Installation
~~~~~~~~~~~~
Install cron_sec like any standard Drupal 6 contrib module.  Place its directory
under sites/all/modules, and then enable the module.  In the course of being
installed, cron_sec will ensure its module weight is the lightest so that it can
execute its security functions as the first module to run via cron.

Configuration
~~~~~~~~~~~~~
To use cron_sec's elevated user permissions feature, you will want to create a
user specifically to be the user running cron, with the permissions you
specifically want that user to have.  Note that this user will also have all
authenticated user permissions.

1.  Create a role called "Cron" (or whatever you want to call it), and assign it
    permissions over and above the authenticated user permissions you want to
    have to run cron.
2.  Create a new user called "cron", and give it the role "Cron" you created in
    the previous step.
3.  Go to cron_sec's configuration page (admin/settings/cron_sec), and specify
    the     new user "cron" as the "CRON user" to be active for cron runs.
    (Leaving this     field blank disables this feature.)
4.  You can also specify a key if you want to be checked when cron runs, or by
    checking the "Generate a random key" checkbox, cron_sec will create one for
    you.
5.  Click the "Save configuration" button.  If you specified your own key or
    asked cron_sec to generate one for you, a confirmation message will show how
    to     configure your call to cron.php properly.
6.  If you are using the key feature, make sure you also configure your call to
    cron.php properly.  For example, the call would look like:
    
        wget -O - -q -t 1 http://example.com/cron.php?key=your_security_key
        
cron_sec will log its results in the log file, so check there to ensure it is
working correctly.
